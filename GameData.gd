extends Node

var begin_data
var begin_satisfaction
var begin_key_data
var win_key_data
var satisfaction_multiplier
var data_win_threshold
var regen_data_timer
var rooms

func _ready():
	print("load data")
	_load_data()

func _error(message):
	GameHelpers._error(message)

func _get_v(key, t):
	if not t.has(key):
		_error("Miss: " + key)
	else:
		return t[key]

func _load_data():
	var file = File.new()
	file.open("user://data.json", file.READ)
	if not file.is_open():
		file.open("/data.json", file.READ)
	if not file.is_open():
		file.open("res://data.json", file.READ)

	var text = file.get_as_text()
	file.close()

	if text == "":
		_error("data file empty")
		return

	var data_file = parse_json(text)

	begin_data = _get_v("begin_data", data_file)
	begin_satisfaction = _get_v("begin_satisfaction", data_file)
	begin_key_data = _get_v("begin_key_data", data_file)
	win_key_data = _get_v("win_key_data", data_file)
	satisfaction_multiplier = _get_v("satisfaction_multiplier", data_file)
	data_win_threshold = _get_v("data_win_threshold", data_file)
	regen_data_timer = _get_v("regen_data_timer", data_file)
	rooms = _get_v("rooms", data_file)

	for room in rooms:
		var room_d = rooms[room]
		
		for room_k in ["unlock", "needs_deck_size", "max_data"]:
			if not room_d.has(room_k):
				room_d[room_k] = 0
				print("Miss key ", room_k, " in the room ", room)
		
		for room_k in ["cards", "needs"]:
			if not room_d.has(room_k):
				room_d[room_k] = []
				print("Miss key ", room_k, " in the room ", room)
			
		for card_d in room_d["cards"]:
			for card_k in ["id", "title", "description", "need"]:
				if not card_d.has(card_k):
					card_d[card_k] = ""
					print("Miss key ", card_k, " in the card ", card_d)
			
			for card_k in ["data_right", "data_wrong", "satisfaction"]:
				if not card_d.has(card_k):
					card_d[card_k] = 0
					print("Miss key ", card_k, " in the card ", card_d)
		
		for need_d in room_d["needs"]:
			for need_k in ["id"]:
				if not need_d.has(need_k):
					need_d[need_k] = ""
					print("Miss key ", need_k, " in the need ", need_d)
