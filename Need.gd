extends Control

signal played

var need setget _set_need
var revealed setget _set_revealed

onready var icone = $"Icone"

func _set_revealed(r):
	revealed = r
	
	if revealed:
		icone.texture = load("res://assets/needs/" + need.id + ".png")
	else:
		icone.texture = load("res://assets/needs/hide.png")
	
	if revealed:
		$Title.text = "RÉVÉLÉ: " + need.id
	else:
		$Title.text = "CACHÉE: " + need.id

func _ready():
	self.need = need

func _set_need(n):
	need = n
	
	visible = need != null
	
	if need == null:
		return
	
	self.revealed = false

func _on_Need_gui_input(ev):
	if ev.is_pressed():
		emit_signal("played")
