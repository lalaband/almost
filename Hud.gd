extends Control

const GameLogic = preload("res://GameLogic.gd")

signal card_played(card)
signal need_played(need)

var _data = 0
var _satisfaction = 0
var _key_data = 0
var _wait_animation = false

onready var _hud_data = $"MarginContainer2/VBoxContainer/HBoxContainer2/Data"
onready var _hud_satisfaction = $"MarginContainer2/VBoxContainer/HBoxContainer/Satisfaction"
onready var _hud_key_data = $"MarginContainer2/VBoxContainer/HBoxContainer3/KeyData"
onready var _cards = $"MarginContainer/HBoxContainer/Cards"
onready var _needs = $"MarginContainer/HBoxContainer/Control/MarginContainer/Needs"
onready var _almost_anim = $"MarginContainer/HBoxContainer/Space/Almost/AnimationPlayer"

func _ready():
	for i in range(GameLogic.DECK_CARDS_SIZE):
		var c = preload("res://Card.tscn").instance()
		c.connect("played", self, "_on_card_played", [c])
		_cards.add_child(c)
	
	for i in range(GameLogic.DECK_NEEDS_SIZE):
		var n = preload("res://Need.tscn").instance()
		n.connect("played", self, "_on_need_played", [n])
		_needs.add_child(n)

func _update_stats():
	_hud_data.text = String(_data)
	_hud_satisfaction.text = String(_satisfaction)
	_hud_key_data.text = String(_key_data)

func _on_data_changed(data):
	_data = data
	_update_stats()

func _on_satisfaction_changed(satisfaction):
	_satisfaction = satisfaction
	_update_stats()

func _on_key_data_changed(key_data):
	_key_data = key_data
	_update_stats()

func _on_card_added(card):
	for hud_card in _cards.get_children():
		if hud_card.card == null :
			hud_card.card = card
			break

func _on_card_removed(card):
	for hud_card in _cards.get_children():
		if hud_card.card != null and hud_card.card.id == card.id:
			hud_card.card = null
			break

func _on_need_added(need):
	for hud_need in _needs.get_children():
		if hud_need.need == null :
			hud_need.need = need
			break

func _on_need_removed(need):
	for hud_need in _needs.get_children():
		if hud_need.need != null and hud_need.need.id == need.id:
			hud_need.need = null
			break


func _on_need_revealed(need):
	for hud_need in _needs.get_children():
		if hud_need.need != null and hud_need.need.id == need.id:
			hud_need.revealed = true
			break

func _on_card_played(hud_card):
	if _wait_animation:
		return
	
	emit_signal("card_played", hud_card.card)
	
func _on_need_played(hud_need):
	if _wait_animation:
		return

	emit_signal("need_played", hud_need.need)


func _on_game_about_to_start():
	for hud_card in _cards.get_children():
		hud_card.card = null
	
	for hud_need in _needs.get_children():
		hud_need.need = null

func _on_Map_animation_started():
	_wait_animation = true

func _on_Map_animation_finished():
	_wait_animation = false




func _on_GameLogic_play_right_card():
	_almost_anim.play("right_card")
	$MarginContainer/HBoxContainer/Space/Almost/AudioRight.play()


func _on_GameLogic_play_wrong_card():
	_almost_anim.play("wrong_card")
	$MarginContainer/HBoxContainer/Space/Almost/AudioWrong.play()
