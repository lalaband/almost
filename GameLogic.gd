extends Node

signal game_about_to_start
signal game_started
signal game_end(has_win)

signal data_changed(data)
signal satisfaction_changed(satisfaction)
signal key_data_changed(key_data)

signal card_added(card)
signal card_removed(card)

signal need_added(need)
signal need_removed(need)
signal need_revealed(need)

signal unlock_room(room)

signal play_right_card
signal play_wrong_card

var _data setget _set_data
var _satisfaction setget _set_satisfaction
var _key_data setget _set_key_data
var _deck_needs_max_size
var _max_data

var _pool_cards
var _pool_needs

const DECK_CARDS_SIZE = 3
const DECK_NEEDS_SIZE = 3

var _deck_cards
var _deck_needs

var _visited_rooms

func _set_data(value):
	if _data == value:
		return
	
	_data = clamp(value, 0.0, _max_data if _max_data != null else 0xFFFFFFFF)
	emit_signal("data_changed", _data)

func _set_satisfaction(value):
	if _satisfaction == value:
		return
	
	_satisfaction = clamp(value, 0.0, 0xFFFFFFFF)
	emit_signal("satisfaction_changed", _satisfaction)

func _set_key_data(value):
	if _key_data == value:
		return
	
	_key_data = value
	emit_signal("key_data_changed", _key_data)

func start_game():
	emit_signal("game_about_to_start")
	
	self._data = GameData.begin_data
	self._satisfaction = GameData.begin_satisfaction
	self._key_data = GameData.begin_key_data
	_deck_needs_max_size = 0
	_max_data = 0
	
	_pool_cards = []
	_pool_needs = []

	_deck_cards = []
	_deck_needs = []
	
	for i in range(DECK_CARDS_SIZE):
		_deck_cards.append(null)
	
	for i in range(DECK_NEEDS_SIZE):
		_deck_needs.append(null)
	
	_visited_rooms = []
	
	_update_game()
	
	emit_signal("game_started")

func play_card(card):
	if _is_need_in_deck(card.need):
		self._satisfaction += card.satisfaction
		self._data += card.data_right
		emit_signal("play_right_card")
	else:
		self._satisfaction -= card.satisfaction
		self._data -= card.data_wrong
		emit_signal("play_wrong_card")
	
	if randf() < GameData.win_key_data:
		self._key_data += 1
	
	for i in range(_deck_needs.size()):
		if _deck_needs[i] != null and _deck_needs[i].id == card.need:
			var c = _deck_needs[i]
			_deck_needs[i] = null
			emit_signal("need_removed", c)
			break
	
	for i in range(_deck_cards.size()):
		if _deck_cards[i] != null and _deck_cards[i].id == card.id:
			emit_signal("card_removed", _deck_cards[i])
			_deck_cards[i] = _get_card()
			emit_signal("card_added", _deck_cards[i])
			break
	
	_update_game()

func reveal_need(need):
	if _key_data < 1:
		return
	
	self._key_data -= 1
	
	emit_signal("need_revealed", need)
	
	_update_game()

# helpers

func _merge_array(a, b):
	for e in b:
		a.append(e)

# game logic

func _update_game():
	_check_game_end()
	_check_new_rooms()
	_update_decks()

func _check_game_end():
	if 1 < _data and _data <= GameData.data_win_threshold and _can_play():
		return
	
	var has_win = 1 < _data
	emit_signal("game_end", has_win)

func _can_play():
	for card in _deck_cards:
		if card != null and card.data_wrong <= _data:
			return true
	
	return false

func _check_new_rooms():
	for room in GameData.rooms:
		var room_d = GameData.rooms[room]
		
		if not _visited_rooms.has(room) and room_d["unlock"] <= _satisfaction:
			_visited_rooms.append(room)
			_deck_needs_max_size = room_d["needs_deck_size"]
			
			if _deck_needs_max_size < 1:
				print("warning: _deck_needs_max_size < 1")
			
			_max_data = room_d["max_data"]
			
			if _max_data < 1:
				print("warning: _max_data < 1")
			
			_merge_array(_pool_cards, room_d["cards"])
			_merge_array(_pool_needs, room_d["needs"])
			
			emit_signal("unlock_room", room)

func _update_decks():
	_fill_deck_cards()
	_fill_deck_needs()

func _fill_deck_cards():
	for i in range(_deck_cards.size()):
		if _deck_cards[i] == null:
			_deck_cards[i] = _get_card()
			emit_signal("card_added", _deck_cards[i])

func _count_needs_in_deck():
	var count = 0
	
	for need in _deck_needs:
		if need != null:
			count += 1
	
	return count

func _fill_deck_needs():
	if _count_needs_in_deck() > 0:
		return
	
	for i in range(_deck_needs_max_size):
		_deck_needs[i] = _get_need()
		emit_signal("need_added", _deck_needs[i])

func _get_card():
#	if _pool_cards.size() < DECK_CARDS_SIZE:
#		_error("can't get card: not enough cards in the set")
#		return
	
	while true:
		var card = _pool_cards[randi() % _pool_cards.size()]
		
		if not _is_card_in_deck(card.id):
			return card

func _get_need():
#	if _pool_needs.size() <= _deck_needs.size():
#		_error("can't get need: not enough needs in the set")
#		return
#
	while true:
		var i = randi() % _pool_needs.size()
		print('get need ', i)
		var need = _pool_needs[i]
		return need

func _is_card_in_deck(card_id):
	for card in _deck_cards:
		if card != null and card.id == card_id:
			return true
	
	return false

func _is_need_in_deck(need_id):
	for need in _deck_needs:
		if need != null and need.id == need_id:
			return true
	
	return false
