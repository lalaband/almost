extends Node

func _on_Hud_card_played(card):
	var p = "res://assets/audio/" + card.id + ".wav"
	
	if not File.new().file_exists(p + ".import"):
		print("warning: audio file ", p, " does not exists")
		return
	
	var s = load(p)
	
	$Player.stream = s
	$Player.play()
