extends Spatial

signal animation_started
signal animation_finished

onready var loft = $"Loft"

var rooms_material = {
	"bathroom": preload("res://bathroom_material.tres"),
	"bedroom": preload("res://bedroom_material.tres"),
	"garage": preload("res://garage_material.tres"),
	"kitchen": preload("res://kitchen_material.tres"),
	"livingroom": preload("res://livingroom_material.tres")
}

func _ready():
	$Loft/AnimationPlayer.connect("animation_started", self, "_on_Loft_animation_started")
	$Loft/AnimationPlayer.connect("animation_finished", self, "_on_Loft_animation_finished")

func _lock_rooms():
	for room in rooms_material:
		rooms_material[room].set_shader_param("saturation_offset", 1.0)

func _on_GameLogic_unlock_room(room):
	var t = Tween.new()
	t.connect("tween_completed", self, "_queue_tween", [t])
	add_child(t)
	t.interpolate_method(self, "_i_" + room, 0.0, 1.0, 2.0, Tween.TRANS_CIRC, Tween.EASE_OUT)
	t.start()

func _on_GameLogic_game_started():
	_lock_rooms()

func _i_bathroom(d):
	_interpolate("bathroom", d)

func _i_bedroom(d):
	_interpolate("bedroom", d)

func _i_garage(d):
	_interpolate("garage", d)

func _i_kitchen(d):
	_interpolate("kitchen", d)

func _i_livingroom(d):
	_interpolate("livingroom", d)

func _interpolate(room, d):
	rooms_material[room].set_shader_param("saturation_offset", 1.0 - d)

func _queue_tween(a, b, tween):
	tween.queue_free()

func _on_Loft_animation_started(anim):
	emit_signal("animation_started")

func _on_Loft_animation_finished(anim):
	emit_signal("animation_finished")

func _on_Hud_card_played(card):
	if $Loft/AnimationPlayer.has_animation(card.id):
		$Loft/AnimationPlayer.play(card.id)
	else:
		print("warning: animation for ", card.id, " does not exist")
