extends Control

signal played

var card setget _set_card

onready var title = $"Control/MarginContainer/VBoxContainer2/VBoxContainer/Title"
onready var need = $"Control/MarginContainer/VBoxContainer2/VBoxContainer/Need"
onready var description = $"Control/MarginContainer/VBoxContainer2/VBoxContainer/Description"
onready var data_right = $"Control/MarginContainer/VBoxContainer2/HBoxContainer/VBoxContainer/DataRight"
onready var data_wrong = $"Control/MarginContainer/VBoxContainer2/HBoxContainer/VBoxContainer/DataWrong"
onready var icone = $"Control/MarginContainer/VBoxContainer2/Icone"
onready var background = $"Control/Background"

func _ready():
	self.card = card

func _set_card(c):
	card = c
	
	visible = card != null
	
	if card == null:
		return
	
	title.text = card.title
	need.text = card.need
	description.text = card.description
	data_right.text = String(card.data_right)
	data_wrong.text = String(card.data_wrong)
	icone.texture = load("res://assets/cards/" + card.id + ".png")
	background.texture = load("res://assets/cards/card_" + card.need + ".png")

func _on_Card_gui_input(ev):
	if ev.is_pressed():
		$AnimationPlayer.play("shake")
#		var offset = 10
#		var time = 1.0
#		$Tween.interpolate_property($body, 'margin_left', 0.0, offset, time, Tween.TRANS_BACK, Tween.EASE_IN)
#		$Tween.interpolate_property($body, 'margin_right', 0.0, offset, time, Tween.TRANS_BACK, Tween.EASE_IN)
#
#		$Tween.interpolate_property($body, 'margin_left', offset, 0, time, Tween.TRANS_BACK, Tween.EASE_OUT, time)
#		$Tween.interpolate_property($body, 'margin_right', offset, 0, time, Tween.TRANS_BACK, Tween.EASE_OUT, time)
#		$Tween.start()
		emit_signal("played")
