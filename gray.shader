shader_type spatial;
render_mode unshaded;

uniform float hue_offset : hint_range(0, 360);
uniform float saturation_offset : hint_range(0, 1);
uniform float value_offset : hint_range(0, 1);

vec3 _hsv2rgb(vec3 hsv) {
	float c = hsv.z * hsv.y;
	float h = hsv.x / 60.0;
	float x = c * float(1 - abs(int(h) % 2 - 1));
	
	vec3 color = vec3(0.0);
	
	if (0.0 <= h && h <= 1.0) {
		color = vec3(c, x, 0.0);
	} else if (1.0 < h && h <= 2.0) {
		color = vec3(x, c, 0.0);
	} else if (2.0 < h && h <= 3.0) {
		color = vec3(0.0, c, x);
	} else if (3.0 < h && h <= 4.0) {
		color = vec3(0.0, x, c);
	} else if (4.0 < h && h <= 5.0) {
		color = vec3(x, 0.0, c);
	} else if (5.0 < h && h <= 6.0) {
		color = vec3(c, 0.0, x);
	}
	
	return color + (hsv.z - c);
}

vec3 _rgb2hsv(vec3 rgb) {
	float v_max = max(rgb.r, max(rgb.g, rgb.b));
	float v_min = min(rgb.r, min(rgb.g, rgb.b));
	
	float h = 0.0;
	
	if (v_max == v_min) {
		h = 0.0;
	} else if (v_max == rgb.r) {
		h = 60.0 * (0.0 + (rgb.g - rgb.b) / (v_max - v_min));
	} else if (v_max == rgb.g) {
		h = 60.0 * (2.0 + (rgb.b - rgb.r) / (v_max - v_min));
	} else if (v_max == rgb.b) {
		h = 60.0 * (4.0 + (rgb.r - rgb.g) / (v_max - v_min));
	}
	
	if (h < 0.0) {
		h += 360.0;
	}
	
	float s = 0.0;
	
	if (v_max != 0.0) {
		s = (v_max - v_min) / v_max;
	}
	
	float v = v_max;
	
	return vec3(h, s, v);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
 
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void fragment() {
	vec3 t = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0).rgb;
	vec3 hsv = rgb2hsv(t);
	hsv -= vec3(hue_offset, saturation_offset, value_offset);
	hsv.x = clamp(hsv.x, 0.0, 360.0);
	hsv.y = clamp(hsv.y, 0.0, 1.0);
	hsv.z = clamp(hsv.z, 0.0, 1.0);
	vec3 rgb = hsv2rgb(hsv);
	ALBEDO = rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
}