extends Control

onready var hud_cards = $MarginContainer/HBoxContainer/Cards
onready var hud_needs = $MarginContainer/HBoxContainer/Needs
onready var hud_info = $Info

func _ready():
	_load_data()
	_connect_signals()
	_start_game()

func _connect_signals():
	for hud_card in hud_cards.get_children():
		hud_card.connect("played", self, "_on_Card_played", [hud_card])
	
	for hud_need in hud_needs.get_children():
		hud_need.connect("played", self, "_on_Need_played", [hud_need])

# helpers


func _merge_array(a, b):
	for e in b:
		a.append(e)

# game logic

var data
var satisfaction
var key_data
var needs_deck_size

var set_cards
var set_needs
var visited_rooms

func _start_game():
	data = begin_data
	satisfaction = begin_satisfaction
	key_data = begin_key_data
	needs_deck_size = 0
	
	set_cards = []
	set_needs = []
	visited_rooms = []
	
	_update_game()
	
func _update_game():
	_update_sets()
	_update_hud()

func _update_sets():
	_check_new_rooms()
	_fill_card_deck()
	_fill_need_deck()

func _check_new_rooms():
	for room in rooms:
		var room_d = rooms[room]
		
		if not visited_rooms.has(room) and room_d["unlock"] <= satisfaction:
			visited_rooms.append(room)
			needs_deck_size = room_d["needs_deck_size"]
			
			if needs_deck_size < 1:
				print("warning: needs_deck_size < 1")
			
			_merge_array(set_cards, room_d["cards"])
			_merge_array(set_needs, room_d["needs"])

func _fill_card_deck():
	for card_hud in hud_cards.get_children():
		if card_hud.card == null:
			card_hud.card = _get_card()

func _count_need_in_deck():
	var count = 0
	
	for need_hud in hud_needs.get_children():
		if need_hud.need != null:
			count += 1
	
	return count

func _fill_need_deck():
	print(_count_need_in_deck())
	if _count_need_in_deck() > 0:
		return
#	for need_hud in hud_needs.get_children():
#		if need_hud.need == null:
#			need_hud.need = _get_need()
	for i in range(needs_deck_size):
		hud_needs.get_child(i).need = _get_need()

func _get_card():
	if set_cards.size() < 4:
		_error("can't get card: not enough cards in the set")
		return
	
	while true:
		var card = set_cards[randi() % set_cards.size()]
		
		if not _is_card_in_deck(card):
			return card

func _get_need():
#	if set_needs.size() < 3:
#		_error("can't get need: not enough needs in the set")
#		return
	
	while true:
		var need = set_needs[randi() % set_needs.size()]
		
		if not _is_need_in_deck(need):
			return need

func _is_card_in_deck(c):
	if c == null:
		return false
	
	for card_hud in hud_cards.get_children():
		if card_hud.card != null and card_hud.card.id == c.id:
			return true
	
	return false

func _is_need_in_deck(n):
	if n == null:
		return false
	
	for need_hud in hud_needs.get_children():
		if need_hud.need != null and need_hud.need.id == n.id:
			return true
	
	return false

func _hide_need_in_deck(need_id):
	for need_hud in hud_needs.get_children():
		if need_hud.need != null and need_hud.need.id == need_id:
			need_hud.need = null
			return
	
	print("can't hide need: ", need_id)

func _update_hud():
	hud_info.text = "DATA: " + String(data) + " SATISFACTION: " + String(satisfaction) + " KEY DATA: " + String(key_data)

func _on_Card_played(hud_card):
	if _is_need_in_deck({"id": hud_card.card.need}):
		satisfaction += hud_card.card.data * satisfaction_multiplier
		data -= hud_card.card.data
		
		if randf() < win_key_data:
			key_data += 1
		
	else:
		satisfaction -= hud_card.card.data * satisfaction_multiplier
		data -= hud_card.card.data
	
	_hide_need_in_deck(hud_card.card.need)
	hud_card.card = _get_card()
	
	_update_game()

func _on_Need_played(hud_need):
	if key_data < 1:
		return
	
	key_data -= 1
	
	_update_game()
