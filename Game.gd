extends Control

onready var _hud = $"Hud"
onready var _map = $"Map"
onready var _win = $"Win"
onready var _lose = $"Lose"
onready var _intro = $Intro

enum State { INTRO, INGAME, WIN, LOSE }

func _ready():
	_change_state(INTRO)

func _start_game():
	$GameLogic.start_game()
	_change_state(INGAME)

func _on_Hud_card_played(card):
	$GameLogic.play_card(card)


func _on_Hud_need_played(need):
	$GameLogic.reveal_need(need)


func _on_GameLogic_game_end(has_win):
	_change_state(WIN if has_win else LOSE)


func _change_state(state):
	_hud.visible = state == INGAME
	_map.visible = state == INGAME
	_win.visible = state == WIN
	_lose.visible = state == LOSE
	_intro.visible = state == INTRO


func _on_WinButton_pressed():
	_start_game()


func _on_LoseButton_pressed():
	_start_game()
